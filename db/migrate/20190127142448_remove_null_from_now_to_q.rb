class RemoveNullFromNowToQ < ActiveRecord::Migration[5.2]
  def up
    # Not Null制約を外す(NULLがOK)
    change_column_null :questionnaires, :from_now, true
  end

  def down
    # Not Null制約を付ける(NULLがNG)
    change_column_null :questionnaires, :from_now, false
  end
end
