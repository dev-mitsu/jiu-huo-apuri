# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_27_142448) do

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "questionnaires", force: :cascade do |t|
    t.integer "school_year", null: false
    t.string "university_name"
    t.integer "conscious_working", null: false
    t.boolean "search_site"
    t.boolean "analysis_myself"
    t.boolean "practice_interview"
    t.boolean "practice_es"
    t.boolean "join_internship"
    t.boolean "attend_job_fair"
    t.boolean "interview_company"
    t.boolean "selection_by_company"
    t.boolean "other"
    t.integer "from_now"
    t.boolean "search_site_no"
    t.boolean "analysis_myself_no"
    t.boolean "practice_interview_no"
    t.boolean "practice_es_no"
    t.boolean "join_internship_no"
    t.boolean "attend_job_fair_no"
    t.boolean "other_no"
    t.datetime "when_to_start"
    t.boolean "nothing_schedule"
    t.integer "anxiety", null: false
    t.boolean "how_to_do"
    t.boolean "start"
    t.boolean "prepare"
    t.boolean "job_offer"
    t.boolean "nothing_to_do"
    t.boolean "decision"
    t.boolean "talk_interview"
    t.boolean "write_es"
    t.boolean "just_because"
    t.boolean "less_info"
    t.boolean "continue_to_work"
    t.boolean "unknown_myself"
    t.boolean "other_anxiety"
    t.text "description_internship"
    t.string "consultation"
    t.boolean "nothing_anxiety"
    t.boolean "all_right"
    t.boolean "join_internship_reason"
    t.boolean "other_no_anxiety"
    t.integer "most_select_reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
