module ApplicationHelper
  def conscious_working_calculation(count)
    q_count = Questionnaire.where(conscious_working: "はい").count
    return_result(count, q_count)
  end

  def q_all_caliculation(count)
    q_count = Questionnaire.count
    return_result(count, q_count)
  end

  def q_5_caliculation(count)
    q_count = Questionnaire.where(conscious_working: "いいえ").count
    return_result(count, q_count)
  end

  def q_6_caliculation(count)
    q_count = Questionnaire.where(from_now: "はい").count
    return_result(count, q_count)
  end

  def q_9_caliculation(count)
    q_count = Questionnaire.where(anxiety: "はい").count
    return_result(count, q_count)
  end

  # Todo: ここだけなぜかto_f(1/0.0)はエラーが発生
  # =>10をlastに変えたら解決原因は不明
  def q_last_caliculation(count)
    q_count = Questionnaire.where(anxiety: "いいえ").count
    return_result(count, q_count)
  end

  def return_result(count, q_count)
    if q_count == 0
      "-"
    else
      (count / q_count.to_f * 100).round(0)
    end
    rescue ZeroDivisionError
      0
  end

end
