class AdminUsersController < ApplicationController
  before_action :authenticate_admin_user!
  def show; end

  def index
    render json: Questionnaire.all
  end

  def table
    @questionnaires = Questionnaire.all
  end
end
